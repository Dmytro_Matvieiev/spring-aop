package spring.aop.aopExample.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spring.aop.aopExample.aspect.LoggedTime;

@Slf4j
@Service
public class ServiceWithAspect {

    @LoggedTime
    public void doSomeWork() {
       log.info("Method is working.");
    }
}
