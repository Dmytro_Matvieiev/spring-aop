package spring.aop.aopExample.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;

@Slf4j
public class LoggerAdvice {
    public void doBefore(JoinPoint joinPoint) {
        log.info("do before: {}", joinPoint.getSignature());
    }
}
